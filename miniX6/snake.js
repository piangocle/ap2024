function Snake() {
    this.x = 0;
    this.y = 0;
    this.xspeed = 1;
    this.yspeed = 0;
    this.total = 0; 
    this.tail = [];

    // checks if the snake is at the same location as the food if yes 1 is added to total 
    this.eat = function (pos) {
        var d = dist(this.x, this.y, pos.x, pos.y);
        if (d < 1) {
            this.total++;
            return true;
        } else {
            return false;
        }
    }

    // made for reciving a x and y value with keyinputs 
    this.dir = function (x, y) {
        this.xspeed = x;
        this.yspeed = y;
    }
    // lopps and checks if the the distance between the head and tail is less than 1 if yes the total resets to 0 and the tail becomes nothing 
    this.gameOver = function() {
        for (var i = 0; i < this.tail.length; i++) {
            var pos = this.tail[i];
            var d = dist(this.x,this.y,pos.x,pos.y);
            if (d < 1) {
                this.total = 0;
                this.tail = [];
            }

        }

    }
    //shifts the array down if the snake has not eaten any food 
    this.update = function () {
        if (this.total === this.tail.length) {
                for (var i = 0; i < this.tail.length-1;i++) {
                    this.tail[i] = this.tail[i+1];
        }
    }
    this.tail[this.total-1] = createVector(this.x,this.y);

        this.x = this.x + this.xspeed * scl;
        this.y = this.y + this.yspeed * scl;

        // contrains the x and y values between 0 and heigth and width - scale 
        this.x = constrain(this.x, 0, width-scl);
        this.y = constrain(this.y, 0, height-scl);
    }
    //
    this.show = function () {
        fill(0, 255, 0);
        for (var i = 0;i < this.tail.length; i++) {
            rect(this.tail[i].x,this.tail[i].y, scl,scl);
        }
        rect(this.x, this.y, scl, scl);
    }}
    