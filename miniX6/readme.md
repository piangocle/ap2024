# MiniX6

<br>

![Før](miniX6.1.png "billed titel")

<br>

Please run the code [here](https://piangocle.gitlab.io/ap2024/miniX6/index.html)

<br>

Please view the full repository [here](https://gitlab.com/piangocle/ap2024/-/tree/main/miniX6)

<br>

I decided to program the classic snake game for the sixth miniX by following a tutorial/coding challenge by the coding train to get a better understanding while programming the game. (https://www.youtube.com/watch?v=AaGK-fj-BAM).

The classic game contains a snake that moves through the screen and eats pieces of food to get longer. If the head of the snake collides with the tail, the game ends or in this case, the tail resets to 0 and the game continues. The game does not have any particular goals for the player other than growing as much as possible.   

the main object of the snake game is the snake itself that the user navigates. The player moves the snake by controlling its head with arrow keys. This is made possible with the function dir and an if else statement. The snake grows longer by colliding with food that randomly generates on the screen with the eat function and if the head collides with the tail the length resets with the gameOver function.

I have learned much about object-oriented programming by working on this miniX and reading the assigned chapter. It simplifies the programming aspect by keeping the code organized. Abstraction makes the complexity of code less by hiding parts from the user and makes it more understandable. Object-oriented programming allows the programmer to easily manipulate functions and objects while making it easier to understand and navigate. 


### **References**

[Coding Challenge #3: The Snake Game](https://www.youtube.com/watch?v=AaGK-fj-BAM)
