var snake
var scl = 50;

var food;

function setup() {
  createCanvas(windowWidth, windowHeight);
  snake = new Snake();
  frameRate(10)
  pickLocation();
}

// picks a random location for the food within the grids 
function pickLocation() {
  var cols = floor(width/scl);
  var rows = floor(height/scl);
  food = createVector(floor(random(cols)),floor(random(rows)));
  food.mult(scl);
}

function draw() {
  background(200);

  if (snake.eat(food)) {
    pickLocation();
  }
  snake.gameOver();
  snake.update();
  snake.show();
  
  fill (255,100,100);
  rect(food.x,food.y,scl,scl);
}
 
// change directions with arrowkeys with the function: dir
function keyPressed() { 
  if (keyCode === UP_ARROW) {
    snake.dir(0, -1);
  } else if (keyCode === DOWN_ARROW) {
    snake.dir(0, 1);
  } else if (keyCode === RIGHT_ARROW) {
    snake.dir(1, 0)
  } else if (keyCode === LEFT_ARROW) {
    snake.dir(-1, 0);
  }
}

