function setup() {
 createCanvas (500,500);

}

function draw() {
  //no "skin color" for the emoji, line thickness and background color change
  background(mouseX)
  noFill()
  strokeWeight(7)

  //black outline emoji
  stroke(0,200)
  ellipse(125,200,200,200);
  ellipse(90,175,20,20);
  ellipse(160,175,20,20);

  stroke(255,0,0,100)
  triangle(125,200,160,240,90,240);

  //white outline emoji 
  stroke(255,200)
  ellipse(375,200,200,200);
  ellipse(340,175,20,20);
  ellipse(410,175,20,20);

  stroke(255,0,0,100)
  triangle(375,250,410,210,340,210);

}


