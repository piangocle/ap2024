# miniX2

<br>

![Før](miniX2.1.png "billed titel")

<br>

Please run the code [here](https://piangocle.gitlab.io/ap2024/miniX2/index.html)

<br>

Please view the full repository [here](https://gitlab.com/piangocle/ap2024/-/tree/main/miniX2)

<br>

I have made a simple program that shows one or two different emojis. The amount of emojis you can see at the same time depends on the background you choose. You change the background from black to white by moving the mouse horizontally. I made the mouth of the emojis red to make sure at least one part of the emoji will always be visible no matter what background the user chooses. 

I deliberately made the emoji as simple as possible because I only wanted them to convey sadness and happiness. The emojis have outlines of opposite colors to make them opposite of each other (besides the emotions) without including race, wealth, or gender. 

I wanted to show that people easily can be overlooked depending on how you choose to view the world, but there will always be a track(?), of someone. 


### **References**

[2.1: Variables in p5.js (mouseX, mouseY) - p5.js Tutorial](https://www.youtube.com/watch?v=7A5tKW9HGoM&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=8)
<br>

[EXECUTING PRACTICES](http://www.data-browser.net/db06.html)
<br>

[Stroke reference](https://p5js.org/reference/#/p5/stroke)
<br>

[Fill reference](https://p5js.org/reference/#/p5/fill)
<br>

[Ellipse reference](https://p5js.org/reference/#/p5/ellipse)

