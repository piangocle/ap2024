# MiniX8

<br>

![Picture](miniX8.1.png "Jelly Bean")

<br>

Please run the code [here](https://astrid-ap.gitlab.io/aestetisk-programmering/MiniX08)

<br>

Please view the full repository [here](https://gitlab.com/astrid-ap/aestetisk-programmering/-/blob/main/MiniX08/README.md)
