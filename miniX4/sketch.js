//declare variable 
let video;

function setup() {
  // put setup code here
  createCanvas(900, 600);
  background(20)
  video = createCapture(VIDEO);
  video.size(900, 600);
  video.hide();
  happyText();
  howAreYou();
}

function draw() {
  image(video, 130, 50);
  happyFace();
  sadFace();
  angryFace();
  happyText();
  frameRate(20);
  howAreYou();
}


function happyFace() {
  //head
  fill(255, 255, 100)
  ellipse(200, 500, 100, 100);
  strokeWeight(3)

  //mouth
  line(180, 520, 200, 530);
  line(220, 520, 200, 530);

  //eyes
  line(170, 500, 190, 500);
  line(230, 500, 210, 500);

}

function sadFace() {
  fill(150, 150, 255)
  ellipse(450, 500, 100, 100);

  line(430, 530, 450, 520);
  line(450, 520, 470, 530);

  line(420, 500, 440, 490);
  line(460, 490, 480, 500);
}

function angryFace() {
  fill(255, 150, 150)
  ellipse(700, 500, 100, 100);

  line(680, 530, 700, 520);
  line(700, 520, 720, 530);

  line(670, 490, 690, 500);
  line(710, 500, 730, 490);
}

function happyText() {
  for (let i = 0; i < 1; i++) {
    let x = random(width); //draws text at random positions
    let y = random(height);
    // Check mouseX position and set fill color accordingly
    if (mouseX > 700) {
      fill(255, 150, 150); // Red color
    } else if (mouseX > 350) {
      fill(150, 150, 255); // Blue color
    } else {
      fill(255, 255, 100); // Yellow color
    }
    
    stroke(3)
    textSize(30);
    text("I'm Happy", x, y)
  }
}


function howAreYou() {
  fill(255, 255, 100)
  stroke(3)
  textSize(30);
  text("How are you?", 370, 580)
}