# Final Project

<br>

Run the code [here](https://aesthetic-programming2981909.gitlab.io/gitlab/FinalProject/miniXFinalProject.html)

View the full repository [here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/tree/main/FinalProject)

ReadMe PDF [here](https://gitlab.com/piangocle/ap2024/-/blob/main/finalProject/Final_Readme.pdf)
