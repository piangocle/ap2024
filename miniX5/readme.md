# Minix5

<br>

![Før](minix5.1.png "billed titel")

<br>

Please run the code [here](https://piangocle.gitlab.io/ap2024/miniX5/index.html)

<br>

Please view the full repository [here](https://gitlab.com/piangocle/ap2024/-/tree/main/miniX5)

<br>

This week's theme for the minix is autogeneration and we had to make a rule-based generative program with at least one loop and conditional statement.  Before I began to code, I devised two simple rules for the program to follow, which are the following:

- a circle/stroke(?) that changes colors when it hits the corner of the screen (limited the color spectrum to greens and blues to make the colors less overwhelming)
- smaller circles attempting to overtake the bigger ones (I tried to change the color of the smaller ones so it would be easier to distinguish the two of them but it did not work)

Generative art is a fairly interesting topic that is different from other art forms like watercolor paint and traditional pencil drawings. The generative art the class is making with p5 uses a different skill set, requiring the artist to have some but not a lot of coding knowledge to create an art piece. I did not know how much i single and simple rule could impact the outcome of an art piece before reading the 5. chapter of the text. It is interesting how a few simple rules can make a complex-looking piece, as shown in the Langton’s ant example from the text. 



### **References**

[For loop random](https://editor.p5js.org/cs4all/sketches/o7Qve2PRY)

<br>
