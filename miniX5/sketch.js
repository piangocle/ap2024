// declaring variables for the circle 
var x = 0;
var y = 0;
var speed = 100;
var gravity = 100;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(100)

}

function draw() {
  ellipse(x, y, 250,);
  noStroke()

  x = x + speed
  y = y + gravity

  //if circle x value is greater width or less than 0 change speed 
  if (x > width || x < 0) {
    speed *= -1;
    fill (0,random(100,255),random(100,255))
  }
  //if circle y value is greater heitht or less than 0 change speed 
  if (y > height || y < 0) {
    gravity *= -1; 
    fill (0,random(100,255),random(100,255))
  }

  for (var i=0; i<random(200,400); i++){ // repeat the loop a specific number of times to draw a specific amount of ellipses
    ellipse(random(width), random(height), 20);
  }

}
