# MiniX1

<br>

![Før](miniX1.1.png "billed titel")

![Efter](miniX1.2.png "billed titel")
<br>

Please run the code [here](https://piangocle.gitlab.io/ap2024/miniX1.2/index.html)

<br>
Please view the full repository[here] (https://gitlab.com/piangocle/ap2024/-/tree/main/miniX1.2)

<br>


My first project with p5 is a red circle with a green outline in a green void. The canvas background changes when someone moves the mouse across it by drawing blue rectangles with a red outline at the mouse’s coordinates. 

It was fun to have the freedom to program whatever I wanted, even though it didn’t always turn out as I expected. The coloring of the different shapes is a good example. I struggled earlier with the color because I didn’t know what order I had to write the code, but I still managed to figure it out. Modifying the different code when I looked at the references was fun. I do somewhat enjoy writing code but I did have some problems earlier when I had to write the function createCanvas because I forgot the uppercase C in canvas.

Reading and writing text feel different from reading and writing code, even though they are similar in many ways. It is like I am learning a new language when I am working with code. The process of reading and writing is slow for now because I have to make sure the code has been written correctly. 

Code and programming can be fun but also tedious. Small errors in the code can lead to the code not working at all, but solving that problem feels great every time. 

![Efter2](miniX1.3.png "billed titel")
<br>
I have revisted my miniX1. The code is mostly the same but i have changed the look of the program to make it more fun to use. 

### **References**

[Drawing Shapes in p5.js for Beginners (1.3) by Codingtrain](https://www.youtube.com/watch?v=c3TeLi6Ns1E&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=4)
<br>
[P5 Get started page](https://p5js.org/get-started/)
<br>
[Stroke reference](https://p5js.org/reference/#/p5/stroke)
<br>
[Fill reference](https://p5js.org/reference/#/p5/fill)
<br>
[Ellipse reference](https://p5js.org/reference/#/p5/ellipse)

