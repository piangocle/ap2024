# MiniX3

<br>

![Før](miniX3.1.png "billed titel")

<br>

Please run the code [here](https://piangocle.gitlab.io/ap2024/miniX3/index.html)

<br>

Please view the full repository [here](https://gitlab.com/piangocle/ap2024/-/tree/main/miniX3)

<br>

I chose to design a throbber that resembles a flower that is losing its leaves, as a representation of time passing by. Most flowers lose their petals as the seasons go by, which indicates a large passing of time. Throbbers are often used in the digital world to inform the user that the computer is thinking while the user is waiting, normally consisting of grey circles. A flower was chosen to make the usually boring throbber more exciting and pretty to look at. 

Computers nowadays think incredibly fast, yet the throbber icons we often encounter daily are often considered an annoyance or inconvenience since they hinder the user from performing any actions. 

I chose to give the throbber animation a framerate of 13 and give the background an alpha value of 80 to make the flower lose its petals quickly to show time passing by quickly. The alpha value makes it so that all of the flower petals never are visible at the same time and slowly fade after being drawn in the function. 



### **References**

[Rotate reference](https://p5js.org/reference/#/p5/rotate)

[FrameRate reference](https://p5js.org/reference/#/p5/frameRate)
<br>

[Pop reference](https://p5js.org/reference/#/p5/pop)
<br>

[Push reference](https://p5js.org/reference/#/p5/push)
<br>

