//Create an empty array to store the stars
let stars = []

function setup() {
  createCanvas(windowWidth, windowHeight);
  //changes how fast the buffer goes
  frameRate(13);

  	//Loop through however many stars you want
	for (i = 0; i < 500; i++){
		
		// Create a new 'star' object, to store an X and Y value.
		// Assign each X and Y value a random number within the 
		// bounds of the sketch.
		
		// This is not really an object, just a short way to store
		// 2 variables (since we need both x and y)
		
		let star = {
			x:random(0,width),
			y:random(0,height)
		};
		
		// Add this 'star' to the array from before.
		stars.push(star);		
	}

}
function draw() {
  background(20, 30, 30, 80); //(the squares are only drawing once, they only lose value)
  drawElements();
	// Loop through all the stars in the array
	for (i = 0; i < 500; i++){
		
		// Define local variables based on the X and Y values of *this* star.
		// This is not strictly necessary, but it makes the code a bit
		// easier to read. 
		let x = stars[i].x;
		let y = stars[i].y;
		
		fill(255);
		
		// Draw an ellipse with this particular star's X and Y value,
		// Also, choose a random size fore this frame. The random size 
		// is what gives us the glittery effect.
		
		ellipse(x,y,random(1,3),random(1,3));
  }

}
function drawElements() {
  let num = 10; //changes the amount of cirkles in the thorbber
  push();
  translate(width / 2, height / 2); 

  let cir = 360 / num * (frameCount % num);  //changes the circles radius 
  rotate(radians(cir));
  noStroke();
  fill(255, 250, 150);
  // the x parameter is the ellipse's distance from the center
  ellipse(0, 0, 55, 55,);

  fill(255, 250, 255);
  rect(20, 0, 70, 50, 50);

  pop();
  // static lines 
  stroke(255, 250, 255, 18);
  line(60, 0, 60, height);
  line(width - 60, 0, width - 60, height);
  line(0, 60, width, 60);
  line(0, height - 60, width, height - 60);

}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}